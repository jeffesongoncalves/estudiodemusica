<?php
    include "Conexao.php";
    class Pessoa extends Conexao{
        private $id;
        private $nome;
        private $telefone;
        private $email;
        private $genero;
        private $sexo;
    
        public function __construct(){
            parent::__construct();
        }
    
        public function __set($p,$v){
            $this->$p = $v;
        }
    
        public function __get($p){
            return $this->$p;
        }
        public function salvar(){
            $sql = "INSERT INTO musico(nome,telefone,email,genero,sexo) VALUES(:nome, :telefone,:email,:genero,:sexo)";
            $param = array(":nome" => $this->nome, 
                           ":telefone"=> $this->telefone,
                            ":email"=>$this->email,
                            ":genero"=>$this->genero,
                            ":sexo"=>$this->sexo);
            $id = parent::inserir($sql,$param);
            $this->id = $id;
            echo "Inserido: ".$id;
        }
    
        public function alterar(){
            $sql= "UPDATE musico SET nome=:nome,telefone=:telefone,,email=:email,genero=:genero,sexo=:sexo WHERE id=:id";
            $param = array(":nome" =>$this->nome,
                            ":telefone"=>$this->telefone,
                            ":email"=>$this->email,
                            ":genero"=>$this->genero,
                            ":sexo"=>$this->sexo,
                            ":id"=>$this->id);
            $atualizando = parent::atualizar($sql,$param);
            echo "Atualizado ".$atualizando;
        }
        
        public function excluir(){
            $sql = "DELETE FROM musico WHERE id = :id";
            $param = array(":id" => $this->id);
            $excluido = parent::apagar($sql, $param);
            echo "Excluido: ".$excluido;
        }
    
        public function mostrar(){
            $slq = "SELECT * FROM musico WHERE id=:id";
            $param = array(":id" => $this->id);
            $sel = parent::listar($sql, $param);
            echo "Listas <br> ";
        }

    }
?>