-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 18-Jun-2018 às 20:46
-- Versão do servidor: 10.1.26-MariaDB-0+deb9u1
-- PHP Version: 7.0.27-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `estudio`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `musico`
--

CREATE TABLE `musico` (
  `id` int(11) NOT NULL,
  `nome` varchar(15) NOT NULL,
  `telefone` varchar(15) NOT NULL,
  `email` varchar(15) NOT NULL,
  `genero` varchar(15) NOT NULL,
  `sexo` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `musico`
--

INSERT INTO `musico` (`id`, `nome`, `telefone`, `email`, `genero`, `sexo`) VALUES
(1, 'Jef', '033988923848', 'jef@gmail.com', 'Rock', 'M'),
(3, 'Fulano', '033988123768', 'fulano@gmail.co', 'axe', 'M');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `musico`
--
ALTER TABLE `musico`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `musico`
--
ALTER TABLE `musico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
